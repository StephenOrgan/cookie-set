<script>

(function(){ 

if ("{{infl_share_token}}" != 'undefined' && "{{infl_referral_campaign_id}}" != 'undefined'){
	var cookieName  = "infl_referral_campaign_id"; 
	var cookieValue = "{{infl_referral_campaign_id}}";
	var otherCookieName = "infl_share_token";
	var otherCookieValue = "{{infl_share_token}}";
	var cookiePath  = "/";
	 
	var expirationTime = 2628000;                           //For example one month in seconds (2628000)
	expirationTime = expirationTime * 1000;                 //Convert expirationtime to milliseconds
	 
	var date = new Date();                                  //Create javascript date object
	var dateTimeNow = date.getTime();                       //Get current time in milliseconds since 1 january 1970 (Unix time)
	 
	date.setTime(dateTimeNow + expirationTime);             //Set expiration time (Time now + one month)
	 
	var expirationTime = date.toUTCString();                //Convert milliseconds to UTC time string
	 
	document.cookie = cookieName+"="+cookieValue+"; expires="+expirationTime+"; path="+cookiePath;  //Set cookie
	document.cookie = otherCookieName+"="+otherCookieValue+"; expires="+expirationTime+"; path="+cookiePath;  
}

})();


</script>